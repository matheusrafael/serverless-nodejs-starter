module.exports = {
  printWidth: 90,
  arrowParens: 'avoid',
  bracketSpacing: true,
  semi: true,
  singleQuote: true,
  trailingComma: 'none'
};
