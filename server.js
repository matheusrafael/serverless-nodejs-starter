/* eslint-disable no-unused-vars */
import { serverlessApi } from './src/app';

export const handler = async (event, context) => {
  return {
    statusCode: 200,
    body: JSON.stringify({
      message: `Go Serverless v1.0! ${(await message({ time: 1, copy: 'Your function executed successfully!'}))}`,
    }),
  };
};

export const app = serverlessApi;
// export const app = async (event, context) => {
//   console.log({ event, context });
//   return serverlessApi;
// };

const message = ({ time, ...rest }) => new Promise((resolve, reject) =>
  setTimeout(() => {
    resolve(`${rest.copy} (with a delay)`);
  }, time * 1000)
);
