/* eslint-disable no-console */
/* eslint-disable max-statements */
import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import serverless from 'serverless-http';
import bearerToken from 'express-bearer-token';
import routesIndex from './routes';
import { routerAuth } from './routes/auth.route';
import { routerBolsa } from './routes/bolsa.route';
import { routerBolsas } from './routes/bolsas.route';
import { routerDoacao } from './routes/doacao.route';
import { routerDoacoes } from './routes/doacoes.route';
import { routerDoador } from './routes/doador.route';
import { routerDoadores } from './routes/doadores.route';
import { routerUnidade } from './routes/unidade.route';
import { routerUnidades } from './routes/unidades.route';
import { routerVisita } from './routes/visita.route';
import { routerVisitas } from './routes/visitas.route';

// initialize app
const app = express();

// view engine setup
// app.set('views', path.join(__dirname, 'views'));
// app.set('view engine', 'jade');

app.use(cors());
app.use(
  bodyParser.json({
    limit: '128mb',
  }),
);
app.use(bearerToken());

// app.get('/', (req, res) => {
//   console.log('sdasdad');
//   return res.send({
//     message: 'Data upload API',
//   });
// });

app.use('/', routesIndex);
app.use('/auth', routerAuth);
app.use('/bolsa', routerBolsa);
app.use('/bolsas', routerBolsas);
app.use('/doacao', routerDoacao);
app.use('/doacoes', routerDoacoes);
app.use('/doador', routerDoador);
app.use('/doadores', routerDoadores);
app.use('/unidade', routerUnidade);
app.use('/unidades', routerUnidades);
app.use('/visita', routerVisita);
app.use('/visitas', routerVisitas);

export async function appHandler(event, context) {
  console.log('evento acontecendo aqui viu!');
  return { event, context };
}

const appApi = async () => app;
export const serverlessApi = serverless(app);
export default appApi;
