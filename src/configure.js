import { config } from 'dotenv';
import schemaJson from './swagger.json';

export const getPath = () => {
  // console.log();
  if (process.env.NODE_ENV === 'production') {
    return {
      path: '.env.prod'
    };
  }

  if (process.env.NODE_ENV === 'development') {
    return {
      path: '.env.dev'
    };
  }

  if (process.env.NODE_ENV === 'test') {
    return {
      path: '.env.test'
    };
  }
  return { path: '.env' };
};

export const configure = () => {
  const result = config(getPath());

  if (result.error) {
    // console.log(result.error);
  }

  const {
    BASE_URL: baseURL,
    PORT: port = 3000,
    NODE_ENV: nodeEnv = 'development',
    STAGE: stage = '',
    ENDPOINT_PREFIX: endpointPrefix = '',
    VALIDATION: localValidation = 0,
    // aws
    REGION: region = 'us-east-1',
    POLL_REGION: pollRegion,
    // cognito
    USER_POLL_ID: userPoolId,
    CLIENT_ID: clientId,
    ARN_GROUP: arnGroup,
    // RDS
    RDS_HOSTNAME: rdsHostname,
    RDS_USERNAME: rdsUsername,
    RDS_PASSWORD: rdsPassword,
    RDS_DATABASE: rdsDatabase,
    RDS_PORT: rdsPort,
    // project
    PROJECT_ID: loggerProjectId = 'data-upload-api',
    LOGGER_LOGNAME: loggerLogName = 'dua-logger',
    LOGGER_SHORTNAME: loggerShortname = 'sba',
    LOG_CONSOLE: logInConsole = 1
  } = process.env;

  const structuredSettings = {
    baseURL,
    endpointPrefix,
    port,
    localValidation,
    env: {
      nodeEnv,
      stage
    },
    aws: {
      region,
      cognito: {
        pollRegion,
        userPoolId,
        clientId,
        arnGroup
      },
      rds: {
        rdsHostname,
        rdsUsername,
        rdsPassword,
        rdsDatabase,
        rdsPort
      }
    },
    logs: {
      projectId: loggerProjectId,
      shortname: loggerShortname,
      logName: loggerLogName,
      logInConsole: parseInt(logInConsole, 10)
    }
  };
  return structuredSettings;
};

export const schema = () => schemaJson;

export default configure;
