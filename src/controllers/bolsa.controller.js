/* eslint-disable no-unused-vars */

import Bolsa from '../models/bolsa.model';
import { debug } from '../util/logger';

export const getAll = async (order = 'id') => {
  const bolsa = await Bolsa.query().orderBy(order);
  debug('bolsa', bolsa);
  return bolsa;
};

export const getById = async id => {
  const bolsa = await Bolsa.query()
    .select()
    .andWhere({
      id
    });
  debug('bolsa', bolsa);
  return bolsa;
};

export const getByName = async name => {
  const bolsa = await Bolsa.query()
    .select()
    .andWhere({
      name
    });
  debug('bolsa', bolsa);
  return bolsa;
};

export const createBolsa = async data => {
  const bolsa = await Bolsa.query().insertAndFetch(data);
  debug('bolsa', bolsa);
  return bolsa;
};

export const updateById = async (id, data) => {
  const bolsa = await Bolsa.query().patchAndFetchById(id, data);
  debug('bolsa', bolsa);
  return bolsa;
};

export const deleteById = async id => {
  const bolsa = await Bolsa.query().deleteById(id);
  debug('bolsa', bolsa);
  return bolsa;
};
