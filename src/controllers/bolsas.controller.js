/* eslint-disable no-unused-vars */

import Bolsas from '../models/bolsas.model';
import { debug } from '../util/logger';

export const getAll = async (order = 'id') => {
  const bolsas = await Bolsas.query().orderBy(order);
  debug('bolsas', bolsas);
  return bolsas;
};

export const getById = async id => {
  const bolsas = await Bolsas.query()
    .select()
    .andWhere({
      id
    });
  debug('bolsas', bolsas);
  return bolsas;
};

export const getByName = async name => {
  const bolsas = await Bolsas.query()
    .select()
    .andWhere({
      name
    });
  debug('bolsas', bolsas);
  return bolsas;
};

export const createBolsas = async data => {
  const bolsas = await Bolsas.query().insertAndFetch(data);
  debug('bolsas', bolsas);
  return bolsas;
};

export const updateById = async (id, data) => {
  const bolsas = await Bolsas.query().patchAndFetchById(id, data);
  debug('bolsas', bolsas);
  return bolsas;
};

export const deleteById = async id => {
  const bolsas = await Bolsas.query().deleteById(id);
  debug('bolsas', bolsas);
  return bolsas;
};
