/* eslint-disable no-unused-vars */

import Doacao from '../models/doacao.model';
import { debug } from '../util/logger';

export const getAll = async (order = 'id') => {
  const doacao = await Doacao.query().orderBy(order);
  debug('doacao', doacao);
  return doacao;
};

export const getById = async id => {
  const doacao = await Doacao.query()
    .select()
    .andWhere({
      id
    });
  debug('doacao', doacao);
  return doacao;
};

export const getByName = async name => {
  const doacao = await Doacao.query()
    .select()
    .andWhere({
      name
    });
  debug('doacao', doacao);
  return doacao;
};

export const createDoacoes = async data => {
  const doacao = await Doacao.query().insertAndFetch(data);
  debug('doacao', doacao);
  return doacao;
};

export const updateById = async (id, data) => {
  const doacao = await Doacao.query().patchAndFetchById(id, data);
  debug('doacao', doacao);
  return doacao;
};

export const deleteById = async id => {
  const doacao = await Doacao.query().deleteById(id);
  debug('doacao', doacao);
  return doacao;
};
