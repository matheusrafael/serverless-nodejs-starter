/* eslint-disable no-unused-vars */

import Doacoes from '../models/doacoes.model';
import { debug } from '../util/logger';

export const getAll = async (order = 'id') => {
  const doacoes = await Doacoes.query().orderBy(order);
  debug('doacoes', doacoes);
  return doacoes;
};

export const getById = async id => {
  const doacoes = await Doacoes.query()
    .select()
    .andWhere({
      id
    });
  debug('doacoes', doacoes);
  return doacoes;
};

export const getByName = async name => {
  const doacoes = await Doacoes.query()
    .select()
    .andWhere({
      name
    });
  debug('doacoes', doacoes);
  return doacoes;
};

export const createDoacoes = async data => {
  const doacoes = await Doacoes.query().insertAndFetch(data);
  debug('doacoes', doacoes);
  return doacoes;
};

export const updateById = async (id, data) => {
  const doacoes = await Doacoes.query().patchAndFetchById(id, data);
  debug('doacoes', doacoes);
  return doacoes;
};

export const deleteById = async id => {
  const doacoes = await Doacoes.query().deleteById(id);
  debug('doacoes', doacoes);
  return doacoes;
};
