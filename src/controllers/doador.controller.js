/* eslint-disable no-unused-vars */

import Doador from '../models/doador.model';
import { debug } from '../util/logger';

export const getAll = async (order = 'id') => {
  const doador = await Doador.query().orderBy(order);
  debug('doador', doador);
  return doador;
};

export const getById = async id => {
  const doador = await Doador.query()
    .select()
    .andWhere({
      id
    });
  debug('doador', doador);
  return doador;
};

export const getByName = async name => {
  const doador = await Doador.query()
    .select()
    .andWhere({
      name
    });
  debug('doador', doador);
  return doador;
};

export const createDoadores = async data => {
  const doador = await Doador.query().insertAndFetch(data);
  debug('doador', doador);
  return doador;
};

export const updateById = async (id, data) => {
  const doador = await Doador.query().patchAndFetchById(id, data);
  debug('doador', doador);
  return doador;
};

export const deleteById = async id => {
  const doador = await Doador.query().deleteById(id);
  debug('doador', doador);
  return doador;
};
