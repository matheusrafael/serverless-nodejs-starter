/* eslint-disable no-unused-vars */

import Doadores from '../models/doadores.model';
import { debug } from '../util/logger';

export const getAll = async (order = 'id') => {
  const doadores = await Doadores.query().orderBy(order);
  debug('doadores', doadores);
  return doadores;
};

export const getById = async id => {
  const doadores = await Doadores.query()
    .select()
    .andWhere({
      id
    });
  debug('doadores', doadores);
  return doadores;
};

export const getByName = async name => {
  const doadores = await Doadores.query()
    .select()
    .andWhere({
      name
    });
  debug('doadores', doadores);
  return doadores;
};

export const createDoadores = async data => {
  const doadores = await Doadores.query().insertAndFetch(data);
  debug('doadores', doadores);
  return doadores;
};

export const updateById = async (id, data) => {
  const doadores = await Doadores.query().patchAndFetchById(id, data);
  debug('doadores', doadores);
  return doadores;
};

export const deleteById = async id => {
  const doadores = await Doadores.query().deleteById(id);
  debug('doadores', doadores);
  return doadores;
};
