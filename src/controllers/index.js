/* eslint-disable no-unused-vars */

// const User from '../models/user.model');
import AWS from 'aws-sdk';
import AmazonCognitoIdentity from 'amazon-cognito-identity-js';
import { debug } from '../util/logger';

const poolData = {
  UserPoolId: 'us-east-1_LZhcGqbay',
  ClientId: '18f84gp99ctc8fgllstjldnhuo'
};

const userPool = new AmazonCognitoIdentity.CognitoUserPool(poolData);

const userData = {
  Username: 'matheus.lima@savelivez.com',
  Password: 'Save@livez123#!',
  Pool: userPool
};
const cognitoUser = new AmazonCognitoIdentity.CognitoUser(userData);

const attributeList = [];

export const login = async () => {
  const atbList = [];

  const dataEmail = {
    Name: 'email',
    Value: 'email@mydomain.com'
  };

  const dataPhoneNumber = {
    Name: 'phone_number',
    Value: '+15555555555'
  };

  const attributeEmail = new AmazonCognitoIdentity.CognitoUserAttribute(dataEmail);
  const attributePhoneNumber = new AmazonCognitoIdentity.CognitoUserAttribute(
    dataPhoneNumber
  );

  atbList.push(attributeEmail);
  atbList.push(attributePhoneNumber);
  userPool.signUp('username', 'password', attributeList, null, (err, result) => {
    if (err) {
      console.log(err.message || JSON.stringify(err));
      return;
    }
    const cgtUser = result.user;
    console.log(`user name is ${cgtUser.getUsername()}`);
  });
};

export const Auth = async () => {
  return { userPool, cognitoUser };
};

export const Login = async (
  email = 'matheus.lima@savelivez.com',
  password = 'Save@livez123#!'
) => {
  const authenticationData = {
    Username: email,
    Password: password
  };

  let userTempData;

  const authenticationDetails = new AmazonCognitoIdentity.AuthenticationDetails(
    authenticationData
  );

  const login = await cognitoUser.authenticateUser(authenticationDetails, {
    async onSuccess(result) {
      const accessToken = await result.getAccessToken().getJwtToken();
      userTempData = accessToken;
      debug('accessToken', accessToken);
      console.log('Successfully logged!');
      return accessToken;
      // // // POTENTIAL: Region needs to be set if not already set previously elsewhere.
      // // AWS.config.region = '<region>';

      // AWS.config.credentials = new AWS.CognitoIdentityCredentials({
      //   IdentityPoolId: poolData.UserPoolId, // your identity pool id here
      //   Logins: {
      //     // Change the key below according to the specific region your user pool is in.
      //     'cognito-idp.us-east-1.amazonaws.com/us-east-1_LZhcGqbay': result
      //       .getIdToken()
      //       .getJwtToken()
      //   }
      // });

      // // refreshes credentials using AWS.CognitoIdentity.getCredentialsForIdentity()
      // AWS.config.credentials.refresh(error => {
      //   if (error) {
      //     console.error(error);
      //   } else {
      //     // Instantiate aws sdk service objects now that the credentials have been updated.
      //     // example: var s3 = new AWS.S3();
      //     console.log('Successfully logged!');
      //   }
      // });
    },

    async onFailure(err) {
      console.error(err.message || JSON.stringify(err));
    },

    async newPasswordRequired(data) {
      console.log(data);
      userTempData = data;
      return data;
      // const oldPass = 'Save@livez123';
      // const newPass = 'Save@livez123#!';

      // const chargePassword = await cognitoUser.changePassword(
      //   oldPass,
      //   newPass,
      //   (err, result) => {
      //     if (err) {
      //       console.error(err.message || JSON.stringify(err));
      //       return;
      //     }
      //     console.log(`call result: ${result}`);
      //   }
      // );

      // debug('chargePassword', chargePassword);
    }
  });

  return login;
};

export const getUserData = async () => {
  const cognitoUserData = await cognitoUser.getUserData;
  return cognitoUserData;
};

export const Register = async (email, password) => {
  const userCredentials = { email, password };
  debug('userCredentials', userCredentials);
  return userCredentials;
};

export const changePassword = async (
  oldPassword = 'Save@livez123#!',
  newPassword = 'Save@livez123#!123'
) => {
  const oldPass = oldPassword;
  const newPass = newPassword;

  const chargePassword = cognitoUser.changePassword(oldPass, newPass, (err, result) => {
    if (err) {
      console.error(err.message || JSON.stringify(err));
      return;
    }
    console.log(`call result: ${result}`);
  });

  debug('chargePassword', chargePassword);
};

// export const getAll = async (order = 'id') => {
//   const user = await User.query().orderBy(order);
//   debug('user', user);
//   return user;
// };

// export const getById = async id => {
//   const user = await User.query()
//     .select()
//     .andWhere({
//       id
//     });
//   debug('user', user);
//   return user;
// };

// export const getByName = async name => {
//   const user = await User.query()
//     .select()
//     .andWhere({
//       name
//     });
//   debug('user', user);
//   return user;
// };

// export const createUser = async data => {
//   const user = await User.query().insertAndFetch(data);
//   debug('user', user);
//   return user;
// };

// export const updateById = async (id, data) => {
//   const user = await User.query().patchAndFetchById(id, data);
//   debug('user', user);
//   return user;
// };

// export const deleteById = async id => {
//   const user = await User.query().deleteById(id);
//   debug('user', user);
//   return user;
// };
