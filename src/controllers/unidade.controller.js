/* eslint-disable no-unused-vars */

import Unidade from '../models/unidade.model';
import { debug } from '../util/logger';

export const getAll = async (order = 'id') => {
  const unidade = await Unidade.query().orderBy(order);
  debug('unidade', unidade);
  return unidade;
};

export const getById = async id => {
  const unidade = await Unidade.query()
    .select()
    .andWhere({
      id
    });
  debug('unidade', unidade);
  return unidade;
};

export const getByName = async name => {
  const unidade = await Unidade.query()
    .select()
    .andWhere({
      name
    });
  debug('unidade', unidade);
  return unidade;
};

export const createUnidades = async data => {
  const unidade = await Unidade.query().insertAndFetch(data);
  debug('unidade', unidade);
  return unidade;
};

export const updateById = async (id, data) => {
  const unidade = await Unidade.query().patchAndFetchById(id, data);
  debug('unidade', unidade);
  return unidade;
};

export const deleteById = async id => {
  const unidade = await Unidade.query().deleteById(id);
  debug('unidade', unidade);
  return unidade;
};
