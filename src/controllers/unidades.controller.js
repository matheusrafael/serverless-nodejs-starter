/* eslint-disable no-unused-vars */

import Unidades from '../models/unidades.model';
import { debug } from '../util/logger';

export const getAll = async (order = 'id') => {
  const unidades = await Unidades.query().orderBy(order);
  debug('unidades', unidades);
  return unidades;
};

export const getById = async id => {
  const unidades = await Unidades.query()
    .select()
    .andWhere({
      id
    });
  debug('unidades', unidades);
  return unidades;
};

export const getByName = async name => {
  const unidades = await Unidades.query()
    .select()
    .andWhere({
      name
    });
  debug('unidades', unidades);
  return unidades;
};

export const createUnidades = async data => {
  const unidades = await Unidades.query().insertAndFetch(data);
  debug('unidades', unidades);
  return unidades;
};

export const updateById = async (id, data) => {
  const unidades = await Unidades.query().patchAndFetchById(id, data);
  debug('unidades', unidades);
  return unidades;
};

export const deleteById = async id => {
  const unidades = await Unidades.query().deleteById(id);
  debug('unidades', unidades);
  return unidades;
};
