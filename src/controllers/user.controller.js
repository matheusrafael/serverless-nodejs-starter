import User from '../models/user.model';
import { debug } from '../util/logger';

export const testUser = () => {
  const user = User;
  debug('user', user);
  return user;
};

export const getAll = async (order = 'id') => {
  const user = await User.query().orderBy(order);
  debug('user', user);
  return user;
};

export const getById = async id => {
  const user = await User.query()
    .select()
    .andWhere({
      id
    });
  debug('user', user);
  return user;
};

export const getByName = async name => {
  const user = await User.query()
    .select()
    .andWhere({
      name
    });
  debug('user', user);
  return user;
};

export const createUser = async data => {
  const user = await User.query().insertAndFetch(data);
  debug('user', user);
  return user;
};

export const updateById = async (id, data) => {
  const user = await User.query().patchAndFetchById(id, data);
  debug('user', user);
  return user;
};

export const deleteById = async id => {
  const user = await User.query().deleteById(id);
  debug('user', user);
  return user;
};
