/* eslint-disable no-unused-vars */

import Visita from '../models/visita.model';
import { debug } from '../util/logger';

export const getAll = async (order = 'id') => {
  const visita = await Visita.query().orderBy(order);
  debug('visita', visita);
  return visita;
};

export const getById = async id => {
  const visita = await Visita.query()
    .select()
    .andWhere({
      id
    });
  debug('visita', visita);
  return visita;
};

export const getByName = async name => {
  const visita = await Visita.query()
    .select()
    .andWhere({
      name
    });
  debug('visita', visita);
  return visita;
};

export const createVisitas = async data => {
  const visita = await Visita.query().insertAndFetch(data);
  debug('visita', visita);
  return visita;
};

export const updateById = async (id, data) => {
  const visita = await Visita.query().patchAndFetchById(id, data);
  debug('visita', visita);
  return visita;
};

export const deleteById = async id => {
  const visita = await Visita.query().deleteById(id);
  debug('visita', visita);
  return visita;
};
