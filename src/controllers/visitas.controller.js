/* eslint-disable no-unused-vars */

import Visitas from '../models/visitas.model';
import { debug } from '../util/logger';

export const getAll = async (order = 'id') => {
  const visitas = await Visitas.query().orderBy(order);
  debug('visitas', visitas);
  return visitas;
};

export const getById = async id => {
  const visitas = await Visitas.query()
    .select()
    .andWhere({
      id
    });
  debug('visitas', visitas);
  return visitas;
};

export const getByName = async name => {
  const visitas = await Visitas.query()
    .select()
    .andWhere({
      name
    });
  debug('visitas', visitas);
  return visitas;
};

export const createVisitas = async data => {
  const visitas = await Visitas.query().insertAndFetch(data);
  debug('visitas', visitas);
  return visitas;
};

export const updateById = async (id, data) => {
  const visitas = await Visitas.query().patchAndFetchById(id, data);
  debug('visitas', visitas);
  return visitas;
};

export const deleteById = async id => {
  const visitas = await Visitas.query().deleteById(id);
  debug('visitas', visitas);
  return visitas;
};
