import { Pool, Client } from 'pg';
import configure from '../configure';

import { debug, info } from '../util/logger';

const {
  aws: {
    rds: { rdsHostname, rdsPort, rdsUsername, rdsPassword, rdsDatabase }
  }
} = configure();

const pollDataConfig = {
  host: rdsHostname,
  user: rdsUsername,
  password: rdsPassword,
  database: rdsDatabase,
  port: rdsPort,
  max: 20,
  idleTimeoutMillis: 30000,
  connectionTimeoutMillis: 2000
};

export const pool = new Pool(pollDataConfig);
export const client = new Client(pollDataConfig);

export const dbPool = {
  client: () => {
    return new Client(pollDataConfig);
  },
  query: async (text, params) => {
    const start = Date.now();
    const result = await pool.query(text, params);
    const duration = Date.now() - start;
    info('duration', duration);
    // info('executed query', {
    //   text,
    //   duration,
    //   result,
    //   command: result.command,
    //   oid: result.oid,
    //   fields: result.fields,
    //   rows: result.rows,
    //   rowCount: result.rowCount
    // });
    return result;
  },
  getClient: callback => {
    debug('callback', callback);
    return pool.connect((err, client, done) => {
      const query = client.query.bind(client);
      // monkey patch the query method to keep track of the last query executed
      // eslint-disable-next-line no-param-reassign
      client.query = () => {
        // client.lastQuery = arguments;
        // client.query.apply(client, arguments);
      };
      // set a timeout of 5 seconds, after which we will log this client's last query
      const timeout = setTimeout(() => {
        console.error('A client has been checked out for more than 5 seconds!');
        console.error(`st executed query on this client: ${client.lastQuery}`);
      }, 5000);
      const release = e => {
        // call the actual 'done' method, returning this client to the pool
        done(e);
        // clear our timeout
        clearTimeout(timeout);
        // set the query method back to its old un-monkey-patched version
        // eslint-disable-next-line no-param-reassign
        client.query = query;
      };
      return callback(err, client, release);
    });
  }
};
