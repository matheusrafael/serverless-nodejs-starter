import knex  from 'knex';
import { configure } from '../configure';
import { debug } from '../util/logger';

const {
  aws: {
    rds: { rdsHostname, rdsUsername, rdsPassword, rdsDatabase, rdsPort }
  }
} = configure();

export const connection = {
  client: 'pg',
  version: '7.2',
  connection: {
    host: rdsHostname,
    user: rdsUsername,
    password: rdsPassword,
    database: rdsDatabase,
    port: rdsPort
  }
};

export const knexInstance = () => knex({
  client: 'pg',
  version: '7.2',
  connection: {
    host: rdsHostname,
    user: rdsUsername,
    password: rdsPassword,
    database: rdsDatabase,
    port: rdsPort
  }
});

// eslint-disable-next-line no-unused-vars
export const up = async (table, data, schema = {}) => {
  debug('knex up', table, data, schema);
  return Promise.all([
    await knex
      .raw(
        `CREATE TABLE IF NOT EXISTS ${table} (id int , name varchar, email varchar)`
      )
      .then(user => {
        // console.log(user);
        return user.rows;
      }),
    await knex(table).insert(data)
  ]);
};

export const down = async table => {
  debug('knex down', table);
  return Promise.all([knex.schema.dropTableIfExists(table)]);
};
