import { Model } from 'objection';
import { knexInstance } from '../database/knex';

Model.knex(knexInstance);

export class Auth extends Model {
  static get tableName() {
    return 'auth';
  }
}
