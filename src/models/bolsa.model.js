import { Model } from 'objection';
import { knexInstance } from '../database/knex';

Model.knex(knexInstance);

export class Bolsas extends Model {
  static get tableName() {
    return 'bolsas';
  }
}
