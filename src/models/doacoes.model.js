import { Model } from 'objection';
import { knexInstance } from '../database/knex';

Model.knex(knexInstance);

export class Doacoes extends Model {
  static get tableName() {
    return 'doacoes';
  }
}
