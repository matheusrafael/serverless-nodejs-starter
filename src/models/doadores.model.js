import { Model } from 'objection';
import { knexInstance } from '../database/knex';

Model.knex(knexInstance);

export class Doadores extends Model {
  static get tableName() {
    return 'Doadores';
  }
}
