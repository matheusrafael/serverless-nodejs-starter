import { Model } from 'objection';
import { knexInstance } from '../database/knex';

Model.knex(knexInstance);

export  class Unidades extends Model {
  static get tableName() {
    return 'unidades';
  }
}
