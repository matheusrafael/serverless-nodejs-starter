import { Model } from 'objection';
import { knexInstance } from '../database/knex';

Model.knex(knexInstance);

export class User extends Model {
  static get tableName() {
    return 'user';
  }
}
