import { Model } from 'objection';
import { knexInstance } from '../database/knex';

Model.knex(knexInstance);

export class Visitas extends Model {
  static get tableName() {
    return 'visitas';
  }
}
