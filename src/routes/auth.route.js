import Router from 'express';
import { debug } from '../util/logger';

import {
  Auth,
  signIn,
  signUp,
  cognitoUserData
} from '../controllers/auth.controller';

const router = Router();

router.get('/', async (req, res) => {
  try {
    const { headers, params } = req;
    // debug('req', req);
    const auth = await Auth();

    const cogUserData = cognitoUserData;

    debug('auth + cogUserData', { auth, cogUserData });

    res.json({ auth, cogUserData, headers, params });
  } catch (error) {
    res.json(error);
  }
});

router.post('/', (req, res) => {
  try {
    const { headers, params, body } = req;
    debug('req', req);
    res.json({ req: { method: 'post' }, headers, params, body });
  } catch (error) {
    res.json(error);
  }
});

router.post('/signin', async (req, res) => {
  try {
    const { headers, params, body } = req;
    const { email, password } = body;
    debug('body', body);
    debug('email, password', { email, password });

    const user = await signIn(email, password);

    res.json({ user, headers, params, body });
  } catch (error) {
    res.json(error);
  }
});

router.post('/signup', async (req, res) => {
  try {
    const { headers, params, body } = req;
    const { email, password } = body;
    debug('email, password', { email, password });
    const user = await signUp(email, password);

    debug('req', req);
    res.json({ user, headers, params, body });
  } catch (error) {
    res.json(error);
  }
});

export const routerAuth = router;
