import Router from 'express';
import { debug } from '../util/logger';

import {
  getAll,
  getById,
  createDoacoes,
  updateById,
  deleteById
} from '../controllers/doacoes.controller';

const router = Router();

router.get('/', async (req, res) => {
  try {
    const { headers, params } = req;
    debug('req', req);

    const doacoesList = await getAll();

    res.json({ headers, params, doacoesList });
  } catch (error) {
    res.json(error);
  }
});

router.get('/:id', async (req, res, next) => {
  try {
    const { headers, params } = req;
    debug('req', req);

    const [doacoesId] = await getById(params.id);
    const { data } = doacoesId;

    return res.json({ headers, params, data });
  } catch (err) {
    return next(err);
  }
});

router.post('/', async (req, res) => {
  try {
    const { headers, params, body } = req;
    debug('req', req);

    const doacoesId = await createDoacoes(body);

    res.json({ headers, params, body, doacoesId });
  } catch (error) {
    res.json(error);
  }
});

router.put('/', async (req, res) => {
  try {
    const { headers, params, body } = req;
    const { id, data } = body;
    debug('req', req);

    const bolsaId = await updateById(id, data);

    res.json({ headers, params, body, bolsaId });
  } catch (error) {
    res.json(error);
  }
});

router.patch('/', (req, res) => {
  try {
    const { headers, params, body } = req;
    debug('req', req);
    res.json({ headers, params, body });
  } catch (error) {
    res.json(error);
  }
});

router.delete('/', async (req, res) => {
  try {
    const { headers, params, body } = req;
    const { id } = body;

    const doacoesId = await deleteById(id);

    debug('req', req);
    res.json({ headers, params, body, doacoesId });
  } catch (error) {
    res.json(error);
  }
});

export const routerDoacoes = router;
