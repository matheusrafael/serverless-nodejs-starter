import Router from 'express';
import { debug } from '../util/logger';

import {
  getAll,
  getById,
  createDoadores,
  updateById,
  deleteById
} from '../controllers/doadores.controller';

const router = Router();

router.get('/', async (req, res) => {
  try {
    const { headers, params } = req;
    debug('req', req);

    const doadoresList = await getAll();

    res.json({ headers, params, doadoresList });
  } catch (error) {
    res.json(error);
  }
});

router.get('/:id', async (req, res, next) => {
  try {
    const { headers, params } = req;
    debug('req', req);

    const [doadoresId] = await getById(params.id);
    const { data } = doadoresId;

    return res.json({ headers, params, data });
  } catch (err) {
    return next(err);
  }
});

router.post('/', async (req, res) => {
  try {
    const { headers, params, body } = req;
    debug('req', req);

    const doadoresId = await createDoadores(body);

    res.json({ headers, params, body, doadoresId });
  } catch (error) {
    res.json(error);
  }
});

router.put('/', async (req, res) => {
  try {
    const { headers, params, body } = req;
    const { id, data } = body;
    debug('req', req);

    const bolsaId = await updateById(id, data);

    res.json({ headers, params, body, bolsaId });
  } catch (error) {
    res.json(error);
  }
});

router.patch('/', (req, res) => {
  try {
    const { headers, params, body } = req;
    debug('req', req);
    res.json({ headers, params, body });
  } catch (error) {
    res.json(error);
  }
});

router.delete('/', async (req, res) => {
  try {
    const { headers, params, body } = req;
    const { id } = body;

    const doadoresId = await deleteById(id);

    debug('req', req);
    res.json({ headers, params, body, doadoresId });
  } catch (error) {
    res.json(error);
  }
});

export const routerDoadores = router;
