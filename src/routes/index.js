import Router from 'express';

const router = Router();

/**
 * @swagger
 * definitions:
 *   Puppy:
 *     properties:
 *       name:
 *         type: string
 *       breed:
 *         type: string
 *       age:
 *         type: integer
 *       sex:
 *         type: string
 */

/**
 * @swagger
 * /api/puppies:
 *   get:
 *     tags:
 *       - Puppies
 *     description: Returns all puppies
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: An array of puppies
 *         schema:
 *           $ref: '#/definitions/Puppy'
 */
// router.get('/', (req, res) => {
//   debug('path', `${__dirname}/public/api-docs/index.html`);
//   res.sendFile(`${__dirname}/public/api-docs/index.html`);
// });

router.get('/', (req, res) => {
  // console.info('req', req);
  res.send('liveZ!');
});

export default router;
