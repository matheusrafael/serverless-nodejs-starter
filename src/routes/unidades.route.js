import Router from 'express';
import { debug } from '../util/logger';

import {
  getAll,
  getById,
  createUnidades,
  updateById,
  deleteById
} from '../controllers/unidades.controller';

const router = Router();

router.get('/', async (req, res) => {
  try {
    const { headers, params } = req;
    debug('req', req);

    const unidadesList = await getAll();

    res.json({ headers, params, unidadesList });
  } catch (error) {
    res.json(error);
  }
});

router.get('/:id', async (req, res, next) => {
  try {
    const { headers, params } = req;
    debug('req', req);

    const [unidadesId] = await getById(params.id);
    const { data } = unidadesId;

    return res.json({ headers, params, data });
  } catch (err) {
    return next(err);
  }
});

router.post('/', async (req, res) => {
  try {
    const { headers, params, body } = req;
    debug('req', req);

    const unidadesId = await createUnidades(body);

    res.json({ headers, params, body, unidadesId });
  } catch (error) {
    res.json(error);
  }
});

router.put('/', async (req, res) => {
  try {
    const { headers, params, body } = req;
    const { id, data } = body;
    debug('req', req);

    const bolsaId = await updateById(id, data);

    res.json({ headers, params, body, bolsaId });
  } catch (error) {
    res.json(error);
  }
});

router.patch('/', (req, res) => {
  try {
    const { headers, params, body } = req;
    debug('req', req);
    res.json({ headers, params, body });
  } catch (error) {
    res.json(error);
  }
});

router.delete('/', async (req, res) => {
  try {
    const { headers, params, body } = req;
    const { id } = body;

    const unidadesId = await deleteById(id);

    debug('req', req);
    res.json({ headers, params, body, unidadesId });
  } catch (error) {
    res.json(error);
  }
});

export const routerUnidades = router;
