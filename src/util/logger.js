export const info = (message, metadata) => {
  const log = console;
  log.info(message, metadata);
};

export const error = (message, metadata) => {
  const log = console;
  log.error(message, metadata);
};

export const warning = (message, metadata) => {
  const log = console;
  log.warn(message, metadata);
};

export const debug = (message, metadata) => {
  const log = console;
  log.debug(message, metadata);
};

const logger = (message, metadata) => {
  const log = console;
  log.debug(message, metadata);
};

export default logger;
