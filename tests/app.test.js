/* eslint-disable no-undef */
import * as app from '../app';

test('app', async () => {
  const event = 'event';
  const context = 'context';
  const callback = (error, response) => {
    expect(response.statusCode).toEqual(200);
    expect(typeof response.body).toBe("string");
  };

  await app.app(event, context, callback);
});
